import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation

def Laplacian(Psi,X):
    LapPsi = (np.roll(Psi,1) + np.roll(Psi,-1) - 2*Psi) / (X[1]-X[0])**2
    #LapPsi = 0*Psi
    #for k in range(1,len(Psi)-1):
    #    LapPsi[k] = (Psi[k+1] - 2*Psi[k] + Psi[k-1]) / (X[1] - X[0])**2
    return LapPsi        

def Hamiltonian(Psi,X,V):
    return -0.5*Laplacian(Psi,X) + V * Psi

def Euler_step(Psi,X,V,dt):
    return -1j * Hamiltonian(Psi,X,V) * dt

def RungeKutta_step(Psi,X,V,dt):
    k1 = Euler_step(Psi,X,V,dt)
    k2 = Euler_step(Psi + 0.5*k1, X, V, dt)
    k3 = Euler_step(Psi + 0.5*k2, X, V, dt)
    k4 = Euler_step(Psi + k3, X, V, dt)
    return (k1 + 2*k2 + 2*k3 + k4) / 6.0

def animate_wavefunction(PsiAll,V,X,scale_factor=60):
    fig, ax = plt.subplots()
    ax.plot(X, V)
    line_abs, = ax.plot(X, scale_factor*np.abs(PsiAll[0]),'k-')
    line_re, = ax.plot(X, scale_factor*np.real(PsiAll[0]),'b-')
    line_im, = ax.plot(X, scale_factor*np.imag(PsiAll[0]),'r-.')

    def ex_ani(i):
        line_abs.set_ydata(scale_factor*np.abs(PsiAll[i]))
        line_re.set_ydata(scale_factor*np.real(PsiAll[i]))
        line_im.set_ydata(scale_factor*np.imag(PsiAll[i]))
        return line_abs, line_re, line_im

    anim = animation.FuncAnimation(fig, ex_ani, interval=100, blit=False, frames=range(len(PsiAll)))
    plt.show()    

def example_spread():
    X = np.linspace(-13,13,501)
    X = X[1:]
    V0 = 30
    V = V0 * (np.heaviside(X - 11.0, 0.5) + np.heaviside(-11.0-X,0.5))

    a = 2.5
    k = 0
    Psi_init = np.exp(-a*(X+2.5)**2) * np.exp(1j * k * X)
    Psi_init = Psi_init * np.sqrt(X[1]-X[0]) / np.linalg.norm(Psi_init)

    nfac = np.vdot(Psi_init, Psi_init)
    T_init = -0.5 * np.vdot(Psi_init,Laplacian(Psi_init,X)) / nfac
    T_pred = (k*k + a)/2
    print('T_init = ',T_init,'   T_pred = ',T_pred)

    dt = 1e-3 * 1/T_init

    Psi = np.copy(Psi_init)
    PsiAll = [np.copy(Psi)]
    for time_step in range(6000):
        Psi = Psi + RungeKutta_step(Psi,X,V,dt)
        Psi = Psi * np.sqrt(X[1]-X[0]) / np.linalg.norm(Psi)
        if time_step % 25 == 0:
            PsiAll.append(np.copy(Psi))        

    # animate
    animate_wavefunction(PsiAll,V,X, 100)


#############
example_spread()
