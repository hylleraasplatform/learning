# Learning

This repository contains teaching material developed at the Hylleraas Center, 
with notebooks that employs the Hylleraas Software Platform.

## Git Guide for Managing Lecture Materials

When working with GitLab, you start by cloning the main repository to you local machine:

```
git clone https://gitlab.com/hylleraasplatform/learning.git
```

Next, make changes in your local repository. When you are ready to share your 
work with others, synchronize these changes with the remote GitLab repository 
using push and pull commands.

### Managing Files
1. **Add New Files:**
   - Place your PDFs, PowerPoint files, and Jupyter notebooks in a suitable directory. 
   - Add each file to Git:
     ```
     git add [filename]
     ```

2. **Commit Changes:**
   - After adding files or making changes, commit them:
     ```
     git commit -m "Add lecture materials for week 1" file1 file2 file3 ...
     ```

3. **View Status:**
   - To check the status of your repository:
     ```
     git status
     ```

4. **Update Existing Files:**
   - Modify your files as needed.
     ```
     git commit -m "Update lecture slides for day 2" file1 file2 file3 ...
     ```

### Syncing with Remote Repository
1. **Pull Changes:**
   - Start by pull the latest version of the main repository first:
     ```
     git pull origin master
     ```
     This ensures that you update your local version with the changes of the 
     work of other lectures. 

2. **Push Changes:**
   - Push your commits to the remote repository:
     ```
     git push origin master
     ```
     After this step, your changes are publically availalbe.

