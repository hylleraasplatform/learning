#hylleraas-school #lecture

[[eletron-dynamics-lecture-outline]]
# Teaching goals

* Learn about the time-dependent Schroedinger equation (TDSE)
* Its uses in quantum chemistry
* Challenges in solving the TDSE on a computer
* Learn about the most popular methods for approximating many-electron dynamics and nuclear ("vibrational") dynamics
* Interpretation of complex-valued wavefunctions
* Be able to relate the TDSE to material previously taught, such as response theory, spectroscopy
* Get hand-on experience with solving TDSE for both simple model problems and actual molecular systems

# Lectures outline

## 1. Time dependent quantum mechanics
1. Complex numbers, recap
2. The time-dependent Schrodinger equation (TDSE)
	1. Presentation of the general equation
		* Including formal solution
		* Context of postulates of nonrelativistic quantum mechanics
	2. The molecular problem, from pre-Born-Oppenheimer, to clamped nuclei (real-time electron dynamics), to Born-Oppenheimer approximation and beyond (nuclear/vibrational dynamics)
	3. Simple 1d and 2d models (atom, barriers, double slit experiment, ...)
3. Interpretation of the time-dependent wavefunction
	1. Born interpretation (probability)
	2. Observables, expectation values
	3. Animated solution of H2+ like dynamics
	5. Animated solution of 2d scattering from cylinder ++
	4. Discussion in groups: Interpretations of animations

## 2. Real-time electron dynamics

1. AO basis sets vs. grids
2. Time-dependent HF and DFT
	1. ReSpect
		1. Scope of calcs with ReSpect?
		2. Hamiltonian is unique, relativistic
		3. Wavefunction, 2c, 4c
		4. Heavy elements, going from simple UV, X, sim of 1 phot proc
		5. Pump probe variants, two laser pulses
		6. Basis sets? AO basis sets.
	2. HyQD nonrel code
		1. Working towards grid based MOs
		
1. **Time-dependent CI**
2. (Time-dependent CC
3. Time-dependent EOMCC)
4. Time-dependent CASSCF (MCTDHF)
	1. AO basis sets vs. grid basis sets for time-dep MO
5. Limits of gaussian basis sets
	* Artificial confinement
	* Lack of continuum description
6. Beyond gaussian basis sets
	* Numerical grids (Finite difference, DVR ...)
	* Orbital adaptivity (HD-HF, ... TD-CASSCF ... OATDCC, ... )

Summary of tools and codes

## 3. Response theory via real-time propagation
1. Adiabatic theorem
2. Ramping of time-dependent perturbation
3. Extraction of response function from real-time propagated signal
4. High-harmonic generation

# Tutorial outline

Idea is to have 2 versions of each exercise: Beginner level, and advanced level. For example, the beginner level could start with an almost complete simulator class. Still plenty of cool stuff to do with that. The advanced level could include implementation of, say, Crank--Nicholson integration, SOFT (split-operator FFT), ...

Exercise 1: 1d model dynamics
- Class implementation of 1d model system with time-dependent Hamiltonian
- Finite difference discretization of an interval
- Compute ground state by diagonalization
- Propagation
	- Direct exponentiation, or scipy ODE solver, whatever is the most convenient and pedagogical
- Plotting of complex wavefunction
- Multiple models
	- scattering against potential barrier
	- 1d H-like system with laser interaction
	- The student can play around
- Probability interpretation:
	- position
	- momentum
	- ionization
	- Compute $\braket{x}$ as function of time
	- Compute HHG spectrum and interpret
- Visualization and interpretation of a complex wavefunctiom
	- Plotting and animation 
	- https://jckantor.github.io/CBE30338/A.03-Animation-in-Jupyter-Notebooks.html


Exercise 2: Real-time electronic structure theory propagation
* Choice of model? Time-dependent CC, EOMCC, or CI (simplest), DFT?
* HyQD? Alternatively, if CI or EOMCC is used, one can have data saved in sparse format on GitLab or similar. Exercise is then coding of a propagator. But it would be much cooler to (a) specify molecule (b) have "black box" functions to generate matrices.
* Specify a molecule
* Specify a laser pulse
* Integrate
* Extract dipole response for varying but small intensities
* Compute linear fit of polarizability
* Compare with response theory "true answer"


