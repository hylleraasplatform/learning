---
number headings: auto, first-level 1, max 6, 1.1
---
#hylleraas-school #hylleraas #teaching

- [1 Time dependent quantum mechanics](#1-time-dependent-quantum-mechanics)
	- [1.1 Outline and Motivation (2-3 slides)](#11-outline-and-motivation-2-3-slides)
	- [1.2 Attosecond chemistry](#12-attosecond-chemistry)
	- [1.3 Ultrashort laser pulses are viewed as important](#13-ultrashort-laser-pulses-are-viewed-as-important)
	- [1.4 An example of ultrafast chemistry](#14-an-example-of-ultrafast-chemistry)
	- [1.5 Complex numbers](#15-complex-numbers)
	- [1.6 Plane waves](#16-plane-waves)
	- [1.7 The wavefunction](#17-the-wavefunction)
	- [1.8 Example: A 2d wavefunction](#18-example-a-2d-wavefunction)
	- [1.9 The time-dependent Schrödinger equation (TDSE)](#19-the-time-dependent-schrödinger-equation-tdse)
	- [1.10 Action principle](#110-action-principle)
	- [1.11 Some model systems (10 min)](#111-some-model-systems-10-min)
		- [1.11.1 Particle in gravity field](#1111-particle-in-gravity-field)
		- [1.11.2 Double-slit experiment](#1112-double-slit-experiment)
		- [1.11.3 Reversibility of TDSE](#1113-reversibility-of-tdse)
	- [1.12 How do we solve the TDSE?](#112-how-do-we-solve-the-tdse)
		- [1.12.1 On the time-independent SE](#1121-on-the-time-independent-se)
		- [1.12.2 Harmonic oscillator eigenstates in 1d and 2d.](#1122-harmonic-oscillator-eigenstates-in-1d-and-2d)
		- [1.12.3 A model atom in an intense laser field](#1123-a-model-atom-in-an-intense-laser-field)
- [2 Many-electron wavefunctions](#2-many-electron-wavefunctions)
	- [2.1 Many-electron wavefunctions](#21-many-electron-wavefunctions)
	- [2.2 Clamped nuclei approximation](#22-clamped-nuclei-approximation)
	- [2.3 The Born--Oppenheimer approximation and our chemical intuition](#23-the-born--oppenheimer-approximation-and-our-chemical-intuition)
- [3 Many-electron dynamics methods](#3-many-electron-dynamics-methods)
	- [3.1 Some references](#31-some-references)
	- [3.2 Uncorrelated wavefunctions: Determinants](#32-uncorrelated-wavefunctions-determinants)
	- [3.3 Time-dependent Hartree--Fock](#33-time-dependent-hartree--fock)
	- [3.4 Comparison of AO vs grid approach](#34-comparison-of-ao-vs-grid-approach)
		- [3.4.1 Visualization: Grid simulation vs. AO simulation](#341-visualization-grid-simulation-vs-ao-simulation)
	- [3.5 Two regimes of applicability](#35-two-regimes-of-applicability)
- [4 Time-dependent electronic structure theory](#4-time-dependent-electronic-structure-theory)
	- [4.1 Time-dependent configuration interaction method](#41-time-dependent-configuration-interaction-method)
	- [4.2 Time-dependent coupled-cluster method](#42-time-dependent-coupled-cluster-method)
- [5 Time-dependent MOs](#5-time-dependent-mos)
	- [5.1 Release the MOs! TD-RASSCF](#51-release-the-mos-td-rasscf)
	- [5.2 OA-TDCC, OO-TDCC](#52-oa-tdcc-oo-tdcc)
	- [5.3 Summing up in a table](#53-summing-up-in-a-table)
- [6 THE END](#6-the-end)


# 1 Time dependent quantum mechanics

## 1.1 Outline and Motivation (2-3 slides)

Link to all material:
- [https://shorturl.at/prtCH](https://shorturl.at/prtCH)
- See  README.md (viewed by default) for direct links to all animations etc.
- This lecture script in markdown/pdf
- Slides
- Tutorials


At the Hylleraas centre, we are a fairly large group of people that study *quantum dynamics* using a variety of different numerical methods. Among the main goals are simulation and understanding of fundamental attosecond experiments and processes from first principles (quoted from RT3/4). The methods we use and study comprise:
- Electron dynamics using standard electronic-structure theory (AO/GTO based) machinery (Software: HyQD)
- Solution of the TDSE on grids and with other non-AO representations, with a goal to solve many-electron problems on large grids (Software: HyQD, other experimental codes)
- Real-time time-dependent SCF methods (Software: ReSpect)
- Born-Oppenheimer molecular dynamics in strong magnetic fields (MAGIC project of Helgaker)
- Going beyond the BO approximation with explicitly correlated gaussians (RT1, RT7)
- These approaches span RT0, RT1, RT3/4, RT7


Today, we will focus on **electron dynamics**. We will talk about the time-dependent Schrödinger equation and its various incarnations. 

## 1.2 Attosecond chemistry

Attosecond chemistry is a field of study within ultrafast science that focuses on investigating and **manipulating chemical reactions at the timescale of attoseconds**. An attosecond is an very brief period of time,

$$ 10^{18} = \frac{1\; \text{attosecond}}{1\; \text{second}} \sim \frac{1\; \text{second}}{1 \; \text{age of universe}} $$

This is the time scale of the electron. Nuclei, on the other hand, move on the femtosecond scale. Which is still fast! But compared to the electrons, the nuclei are virtually stationary.

At the time scale of the electron, various things may happen: ionization, i.e., electrons escaping the system, charge migration due to the electron cloud not being in the ground state anymore, and bond breaking in the sense that electrons are no longer configured so that they bind the nuclei together in the future.

By using extremely short pulses of laser light, is is possible to observe and control the dynamics of **electron movement within molecules** during chemical reactions with unprecedented precision.

If chemistry is defined as the science of breaking and forming chemical bonds, then controlling the electrons responsible for the bonds is the most fundamental way of doing chemistry. (Merritt et al, 2021)

Merritt, I. C. D.; Jacquemin, D.; Vacher, M. Attochemistry: Is Controlling Electrons the Future of Photochemistry? _J. Phys. Chem. Lett._ **2021**, _12_ (34), 8404–8415. [https://doi.org/10.1021/acs.jpclett.1c02016](https://doi.org/10.1021/acs.jpclett.1c02016).


Attosecond chemistry provides insights into fundamental processes such as electron migration, ionization, and bond breaking, leading to a deeper understanding of the dynamics that govern chemical reactions. Attochemistry is an emerging field, and holds promise for advancing our ability to control and optimize matter and chemical reactions at the most fundamental level, potentially opening new avenues for applications in areas such as materials science, catalysis, and quantum information processing.

Thus, in these lectures, the chosen red thread is the study of the dynamics of electronic systems subject to laser pulses.

## 1.3 Ultrashort laser pulses are viewed as important

The Nobel Prize in Physics 2023 was awarded to Pierre Agostini, Ferenc Krausz and Anne L’Huillier "for experimental methods that generate attosecond pulses of light for the study of electron dynamics in matter." (https://www.nobelprize.org/prizes/physics/2023/summary/) 

Laser pulses are indeed important!

In 2018, **Gérard Mourou and Donna Strickland** was awarded the Nobel Prize in physics for chirped-pulse amplification (CPA), a technique that paved the way for the generation of high-intensity **femtosecond** laser pulses.

Only 5 years between laser pulse Nobel Prizes, and the time scales!

## 1.4 An example of ultrafast chemistry

Kaneshima, K.; Ninota, Y.; Sekikawa, T. Time-Resolved High-Harmonic Spectroscopy of Ultrafast Photoisomerization Dynamics. _Optics Express_ **2018**, _26_ (23), 31039. [https://doi.org/10.1364/oe.26.031039](https://doi.org/10.1364/oe.26.031039).


A really interesting example of an ultrafast chemistry experiment is that of Kaneshima et al. (2018), who were the first to use *femtosecond* lasers to watch complex bond rearrangements unfold in time.

They used *Time-Resolved High-Harmonic Spectroscopy*. It is based on *High-Harmonic Generation*:
1. A strong laser field tears a valence electron away from an atom or a molecule through tunnel ionization. 
2. The freed electron is accelerated by the laser electric field and then pulled back to its parent ion when the sign of the electric field reverses. 
3. The laser-driven electron recollides and recombines with its parent ion. As a result of recombination, the sum of the kinetic energy of the recolliding electron and its binding energy is released through *coherent radiation* (i.e., superpositions of the base frequency of the laser, HHG). 
4. This radiation contains information about the electron-ion interaction. Hence, one can retrieve the electronic state of the generating medium from the amplitudes and phases of the high-harmonic spectra. **This is HHS**
5. By controlling the **delay between pump and probe pulses**, we get time-resolved HHS, where snapshots in time can be generated

Although this is really a femtochemistry experiment, it really shows us what we may expect in the future.


## 1.5 Complex numbers

Quantum mechanics is formulated with **complex numbers**. Let us do a quick recap of what complex numbers are.

Complex numbers are an extension of the real numbers. One defines the "imaginary unit" $i$, a number such that $i^2 = -1$, and defines complex numbers to be of the form $z = x + iy$. The number $x$ is the **real part**, while the number $y$ (or sometimes $iy$) is called the **imaginary part**. 

The complex numbers define a **field**: We can add together, multiply, and divide any two numbers (except from zero, that is.) The great bonus of complex numbers is that we now can solve any polynomial system!

Complex numbers are visualized as 2d vectors: 1 and $i$ are basis vectors in the $x$ and $y$ direction, and a complex number is then a point $(x,y)$ in the plane. The difference with vectors in the plane is that we can **multiply and divide** these vectors.

Here are some examples of complex numbers. Note that we can define common functions on complex numbers as well.

An important reformulation of complex numbers is that of **polar form**. Any number $z = x + iy$ can also be written $z = r e^{i\theta}$, where $r$ is the distance from 0, or **modulus/magnitude** $|z|$, and $\theta$ is the angle $z$ makes with the $x$ axis, or **argument/phase** $\arg(z)$. The phase is non-unique.

We will later visualize **complex-valued functions** by using a coloring scheme like this. A color wheel is used to represent the phase of the complex number, while the intensity/saturation is used to visualize magnitude.

## 1.6 Plane waves

An important example of a **complex-valued function** is a **plane wave**: Let $\mathbf{r}$ be a point in space (1d, 2d, or 3d), let $\mathbf{k}$ be a *wave vector*, $\omega$ an angular frequency, and define the function
$$ \psi(\mathbf{r},t) = \exp[i (\mathbf{k}\cdot \mathbf{r} - \omega t)] $$
This is a complex-valued function of space and time. It has modulus $1$, but the phase is not constant and changes in space and time. This function describes a wave wave moving with speed proportional to $\omega$, whose fronts are perpendicular to $\mathbf{k}$. Here is an example: Let $\omega=\pi$,  $\mathbf{k} = (0.75\pi, 1.5\pi)$ in the 2d plane, and let us visualize the plane wave at $t=0$. (Picture.) Then $t=1, 2$. Notice that the lines of constant color mean that the argument (angle) of is constant. These lines are perpendicular to $\mathbf{k}$. 

Notice that, as long as $\omega>0$, we can **read off the direction of travel by looking at the gradient**.

## 1.7 The wavefunction

Quantum mechanics is formulated in terms of the **wave function**, very often denoted by the greek letter Psi $\Psi$. There is a whole lot to be said about the mathematical foundation for this, but we will stick to intuition and very simple mathematics. To the likely chagrin of many in the audience, we will not even formulate the postulates of quantum mechanics.

Let us think for a moment about a single particle living in space. Let us say an electron. A "classical", that is, non-quantum mechanical, electron has a well-defined position and momentum (velocity). Those two properties define the **state** of the electron in classical mechanics. In quantum mechanics, the state is instead a **wavefunction** $\Psi(\mathbf{r},t)$, defined in all space and time. All physical predictions about the electron is encoded in the wavefunction. Thus, it is a kind of oracle. For example, take the **Born rule**:
- $|\Psi(\mathbf{r},t)|^2$ is the *probability* that the electron will be at the spacetime point $(\mathbf{r},t)$.
- The *Fourier transform* of $\Psi$ gives the probability distribution of the momentum!
Thus quantum mechanics has a **probabilistic interpretation.** All observables have similar probability distributions obtained from the wavefunction.

Since the probability distributions are derived from the same mathematical object, it should come as no surprise that they obey constraints. Most importantly, we have the Famous Heisenberg uncertainty relation, that states that the probability distributions of position and momentum are not sharp at the same time. This is the reason why the quantum mechanical atom does not collapse into itself!

There are also other axioms of quantum mechanics, such as what happens when we **measure** observables.

Most importantly, for us, is the **time dependent Schroedinger equation**

## 1.8 Example: A 2d wavefunction


> [!exercise] Discussion in pairs
> We consider an example. Here, you can see a plot of a 2d wavefunction. Recall our color coding of the complex plane. In particular the modulus $|z| = r$ is the length of the vector. Discuss the probability density for locating the particle in space. From what you have seen so far, do you think the particle will move somewhere if we allowed it to propagate according to quantum mechanics?
> 
> 
> 

## 1.9 The time-dependent Schrödinger equation (TDSE) 

Ok, so the wavefunction encodes the state of the electron. But to formulate a natural law, we must also have a notion of how the state moves in time. In classical mechanics, we have Newtonian physics: $\mathbf{F} = m\mathbf{a}$. In quantum mechanics, we have the TDSE. It looks like this:

$$ i \hbar \frac{\partial}{\partial t}\Psi(t) = \hat{H}(t) \Psi(t), $$
Often, as here, we suppress the spatial coordinates in this equation. The thing called $\hat{H}(t)$ is  **the Hamiltonian operator** and represents the total energy of the system, i.e., kinetic and potential energy.
$$ \hat{H}(t) = \hat{T} + \hat{V}(t) $$
The TDSE equation states that the local energy of the system, locally in space and time, tells the wavefunction how to change locally in space and time. 

The Schroedinger equation is a *wave equation* that generalizes Newton's second law (as formulated using Hamiltonian dynamics) to a quantum mechanical laws. Roughly, one can say that "quantum motion is a wave-like probabilistic version of classical motion". To be very concrete, for a single point-like particle in space, moving in a conservative force field defined by $V(\mathbf{r})$, with a possible time-dependence through, say, a time-varying electric field, the TDSE reads
$$  i \hbar \frac{\partial}{\partial t} \Psi(\mathbf{r},t) = -\frac{\hbar^2}{2m} \nabla^2 \Psi (\mathbf{r},t) + V(\mathbf{r},t)\Psi(\mathbf{r},t)
$$
Here $m$ is the mass of the particle.

Don't worry too much about the concrete form of this equation. The point here is that we have an equation that describes how the quantum mechanical state moves in time. Notice also the presence of $i$, which means that $\Psi$ **must be a complex function**.

It is a fact that the plane wave we saw earlier is one particular solution to the TDSE for a particle that travels freely, with no external forces acting on it. (Plane waves are also solutions to other wave equations!)


## 1.10 Action principle

- "For since the fabric of the universe is most perfect, and is the work of a most wise Creator, nothing whatsoever takes place in the universe in which some relation of maximum and minimum does not appear"

Leonhard Euler

It is an almost crazy fact, that *all* laws of nature can be rephrased as optimization problems. Quantum mechanics is no exception, and I would just like to present here the *time-dependent variational principle* (TDVP) which is, almost universally, used to derive approximate equations of motion for quantum mechanical models. The principle is as follows: Let $\Psi(t)$ be a possible history of a quantum system from time $t_0$ to $t_1$. Then, the history that solves the TDSE is the unique history that optimizes the *action functional* $\mathcal{S}$ given by,
$$ \mathcal{S} = \int_{t_0}^{t_1} \braket{\Psi(t)| \left[ i\hbar\frac{\partial}{\partial t} - H(t) \right] |\Psi(t)} \; dt. $$
The functional $\mathcal{S}$ is a scalar quantity that depends on $\Psi(t)$ for all $t$. That the action is optimized means that its derivative with respect to the *whole history* is zero. This, if we perform infinitesimal variations in the *history*,
$$ \Psi(t) \to \Psi(t) + \delta \Psi(t), $$
then the corresponding change in the functional vanishes, $\delta \mathcal{S} = 0$, if and only if $\Psi(t)$ solves the TDSE.

Here, the braket notation denotes the inner product in Hilbert space, where the wavefunction lives.

The significance of the TDVP is that if we have a *model wavefunction*, we can generate a corresponding approximation to the time evolution by optimizing $\mathcal{S}$.

Reference: Lubich, C. _From Quantum to Classical Molecular Dynamics: Reduced Models and Numerical Analysis_; European Mathematical Society, 2008.

## 1.11 Some model systems (10 min)

Let us consider some concrete examples of simple single-particle systems formulated in 2d. We will use these examples to aid our intuition. The TDSE generates fantastically rich, beautiful, and complicated behavior.

The animations shown here are generated with my own software package written in Python. In the tutorials, you will use this software to do some nice simulations and animations.

One point to keep in mind is that in quantum chemistry (i.e., electronic-structure theory), we are most often solving the time-*independent* Schroedinger equation, using a handful of *atomic orbital basis functions*. In the following examples, that would be equivalent to saying that we try to describe the motion of the particle/electron with, say 1 fixed time-independent gaussian function in the minimal basis case, or 4 basis functions in a double zeta basis case. (3 functions in 2d.) I hope you can see that such an approach will *not* fly in this case.

Clearly, much of the intuition used in computational chemistry must be carefully reconsidered.


### 1.11.1 Particle in gravity field


We first discuss the classical mechanics of a single particle in an external potential field. The total energy of a particle is written
$$ E_{tot} = E_{kin} + E_{pot} = \frac{1}{2}m \mathbf{v}^2 + V(\mathbf{r}), $$
where $\mathbf{v}$ is the particle's velocity, and where $V(\mathbf{r})$ is some function, the potential field. Consider for example, a falling point-shaped ball in the $xy$ plane (not to be confused with the complex plane!). Gravity points downwards with magnitude $g$, and the potential energy field is $mgy$. When the ball hits $y=0$, there is a hard floor, so the potential is practically infinite there. We know from experience, that the motion of the particle describes a parabola until $y=0$, where the velocity is reflected, and a new parabola is described. Of course, air resistance and all that, but this is not real life.

What would a *quantum particle's wavefunction* look like if it was in the above described potential field? How would it move? Let us have a look. We start with a concentrated probability distribution of the particle; we know roughly where it is. Notice also the phase -- the particle is moving. We have a *gaussian wavepacket* centered at $\mathbf{r}_0$ with initian momentum expectation value $\mathbf{k}$:

$$ \Psi(\mathbf{r},t) = \mathcal{N} e^{i \mathbf{k}\cdot \mathbf{r}} e^{-(\mathbf{r}-\mathbf{r}_0)^2/(2\sigma)} $$

But look at this time evolution, what happens when the ball hits the floor. Apparently, there are areas, distances from the floor, where the particle *never is*. This is called *self-interference* Notice that we can recognize the classical motion, but that the initial uncertainty in position quickly overtakes any resemblance to classical motion.


> [!exercise] Discussion in pairs
> View the animation, `gravity_complex.mp4`. Discuss the time evolution of the wavefunction, and try to interpret it in terms of both classical intuition and the probability interpretation.
> 
> Pay attention to the phase lines. They tell you something about the particle's momentum (velocity).
> 
> What happens near the ground? Interpret in terms of wave phenomena.
> 
> What happens to the probability distribution after the particle bounces back up?




### 1.11.2 Double-slit experiment

A standard favorite example for animations such as this is *the double slit experiment*. A particle is allowed to move freely in space, incident on a grating with 2 slits. The potential is therefore zero everywhere, except at the walls of the grating where it has a large value.

In the animation you can see how the particle enters *both* slits simultaneously, and that there is a very complicated self-interference pattern. Beyond the grating, the wave self-interferes again, making a characteristic ripple pattern. If we placed a detector at the far side, this pattern would be detectable.

We briefly also show what happens if we close one grate. The interference pattern is more or less gone (we do have *refraction* still).

We conclude that the particle actually goes through both slits at the same time in the first experiment, since the total pattern in that case is not the sum of the single-slit patterns.

The particle exists in *Schrödinger's cat states*, being in multiple classical configurations at the same time. It is not meaningful to say that the particle is here or there - it is everywhere with a probability, and the evolution of that probability depends on the whole probability, so to speak.

### 1.11.3 Reversibility of TDSE

In classical mechanics, Newton's laws are in principle reversible: Start in a state S, given by positions and velocities of the system particles, propagate to a final state F. Reverse velocities of all particles, and propagate forwards in time to obtain the previous state S again.

Of course, statistical mechanics tells us that entropy is increasing. Thus, it is very improbable that you will be able to pick the exact correct initial state to reproduce S, which has lower entropy. For example, you will never see a snow ball reassemble itself from the ground and fly upwards.

The TDSE works similarly. The gaussian wavepacket is very orderly, and upon scattering with its environment becomes very complicated and diffuse. But, in principle, by complex conjugating a final state F, we are reversing the particles' velocities, and propagating forwards in time should produce the initial state S.

This simulation shows this phenomenon. But how difficult do you think it is to prepare this exact initial state from "cratch"?

Animation: `hylleraas.mp4`

## 1.12 How do we solve the TDSE?

We here give a very simplified overview of how the TDSE is solved using grids.

The TDSE is **discretized** in both space and time.

A *space grid* is a discrete point set in space where our approximate wavefunction "lives". We basically say that we only seek our wavefunction on the grid. In our example, we have a **uniform grid**, but there are many other possibilities. With classical electronic-structure methods, one uses AO basis functions, which are very different.

The Hamiltonian operator must be converted from a continuous partial differential operator to something that is "discrete", that acts only on the grid wavefunction. The most important classes are:
- Finite difference method
- Finite element method
- Pseudospectral methods / Discrete variable representation
- Combinations

In our simulations we use the Fourier pseudospectral method.

We also need to introduce a **time grid**, and a method for approximately evolving a grid wavefunction one step at a time. There are many possible choices
* Runge-Kutta integration
* Exponential/Krylov integration
* Splitting methods

In our simulations, we use **Strang splitting**.


The TDSE has several **invariants** that we should try to preserve. The most important are:
- Energy conservation
- Probability conservation

A decent book: Varga, K.; Driscoll, J. A. _Computational Nanoscience: Applications for Molecules, Clusters, and Solids_, 1st ed.; Cambridge University Press, 2011. [https://doi.org/10.1017/CBO9780511736230](https://doi.org/10.1017/CBO9780511736230).

### 1.12.1 On the time-independent SE

Before we view the next example, we should discuss the *time-independent Schrödinger equation* (TISE). It is an eigenvalue problem. Find $E_k\in\mathbb{R}$ and $\Psi_k\neq 0$ such that
$$ \hat{H} \Psi_k = E_k \Psi_k $$
Here, $\hat{H}$ is the time-independent part of the Hamiltonian. (See also later.) In conventional electronic structure theory, *this* is the equation we try to solve. Usually, we seek the *smallest* possible $E_0$ and its $\Psi_0$, that is the ground state energy and the ground state.

Suppose we have found an eigenpair, and use $\Psi_k$ as initial condition for the TDSE. We then get
$$ \Psi(t) = e^{-it E_k/\hbar} \Psi_k $$
as *exact* solution of the TDSE. It is proven simply by inserting it into the TDSE and seeing that it works out.

It is a fact that $e^{-it E_k/hbar}$ is a complex number with modulus 1. Thus,
$$ |\Psi(\mathbf{r},t)|^2 = |\Psi_k(\mathbf{r})|^2 $$
which is *independent of time.* The probability of locating the particle in space does not change. The same is true for *any* probability. Hence, we call $\Psi_k$ a *stationary state*. No physical predictions change in time. Unless we turn on the time-dependent part of the Hamiltonian, that is.

### 1.12.2 Harmonic oscillator eigenstates in 1d and 2d.

The *harmonic oscillator* (HO) is an archetypal quantum mechanical system, since for any potential with a local minimum, the HO represents a first approximation near the minimum.

The HO has Hamiltonian operator
$$ \hat{H} = -\frac{\hbar^2}{2m} \nabla^2 + \frac{1}{2}m\omega^2 |\mathbf{r}|^2 $$

The HO is also important for studying vibrational and rotational states of molecules in the equilibrium geometry. Recall that classically, the particle moves in a force field $-\nabla V(\mathbf{r}) = -m\omega^2 \mathbf{r}$ , which gives classical acceleration $d^2 \mathbf{r}/dt^2 = -\omega^2 \mathbf{r}$. This can be solved to give solutions
$$ \mathbf{r}(r) = A\cos(\omega t) + B\sin(\omega t) $$
that is, harmonic ... oscillations! Think of a spring, where $\omega^2$ is the stiffness of the spring.

The eigenvalues and eigenstates of the HO can be found analytically. We will not list the equations here.

We will now display the time evolution of the eigenstates, first of the 1d HO and then the 2d HO, to illustrate stationary states. Also, we make a connection to how orbitals are often visualized in quantum chemistry.

As we will discuss later, and orbital is a single-electron wavefunction used in a many-electron context. Thus, the orbital is an example of a wavefunction such as we have encountered so far.

In the next few short animations, we see the ground state, the first excited state, and the 10th excited state of the 1d harmonic oscillator.

Next, we show the ground state and some excited states of the 2d harmonic oscillator. Note how the coloring is suggestive of orbital visualizations, where positive and negative isosurfaces are colored in different colors.

In the time-dependent picture, the initially positive and negative values become complex.


### 1.12.3 A model atom in an intense laser field

We now consider a toy model of an atom subject to an intense laser pulse. The model is a *single active electron model*, which basically means that only one electron is described by the quantum dynamics. We also restrict our attention to a 1d model. The particle has mass $m$ and moves in a  potential $V(x)$ given by
$$ V(x) = -\frac{1}{2\sqrt{x^2 + 1/4}} $$
This looks like a Coulomb potential without the sharp singularity, and the ground-state energy of this potential is $-1/2$, just as for the real 3d hydrogen atom. 

The atom is shot at with a laser pulse. We use the **dipole approximation** which means that the wavelength of the laser light is assumed much larger than the extent of the atom, so that the electric field is a function of time $t$ alone, $\mathcal{E}(t)$. The electric field couples to the dipole operator of the atom, here simply $x$. The laser pulse is given by the model 

$$ \mathcal{E}(t) = \mathcal{E}_0 \sin^2\left(\frac{\pi(t-t_0)}{T}    \right) \cos\left(\omega (t - \bar{t})\right), \text{ where } \bar{t} = t_0 + T/2 $$
when $t_0 \leq t \leq t_0 + T$, and zero otherwise.

The TDSE for this system now reads ($\hbar=1$)
$$ i \frac{\partial}{\partial t}\Psi(x,t) = -\frac{1}{2m} \frac{\partial^2}{\partial x^2} \Psi(x,t) + [V(x) - \mathcal{E}(t) x]\Psi(x,t) $$ 
The atom starts out in the **ground state**.

To solve this equation numerically, we must **introduce a spatial discretization** and **a time stepping scheme**. The details are beyond the scope of this presentation, but suffice it to say that we use $n_{grid} = 4096$ grid points in the interval $[-400,400]$. This gives a resolution of $0.195$ atomic units, which is sufficient for this simulation. We use  a time step of $\Delta t = 0.01$


> [!exercise] Discussion in pairs
> Discuss the animation you just saw.
> 
> 1. The wavefunction starts out in the ground state. For how long approximately is the wavefunction in the ground state?
> 2. Describe the wavefunction qualitatively as function of time.
> 3. Discuss the total potential as function of time, plotted in the animation.
> 4. What happens at the peaks and valleys of the laser pulse?
> 5. What do you think is a possible fate of the outgoing "lobes" of the wavefunction? What does this mean (hint: one electron is gone from the atom.)
> 6. Discuss the cost of storing the wavefunction in terms of computer memory, for a single time step. How much would a corresponding 3d simulation cost?

Movie file: `atom_1d.mp4`



# 2 Many-electron wavefunctions


## 2.1 Many-electron wavefunctions

References: Helgaker, T.; Jørgensen, P.; Olsen, J. _Molecular Electronic-Structure Theory_; Wiley: Chichester ; New York, 2000.

Gross, E. K. U.; Runge, E.; Heinonen, O. _Many-Particle Theory_; Adam Hilger: Bristol, Philadelphia and New York, USA, 1991.



It is very nice to see these movies, but they are just toy models. We are quantum chemists, and want do study molecules! So let us ask: what is the wavefunction of a molecule?

(As an aside ...  For example, the double slit experiment has been performed with rather large molecules, even microbes, so can we consider the quantum mechanical state to simply be a wavefunction $\Psi(\mathbf{r},t)$? What about its internal motion or the feelings of the microbe ...? This is complicated, and I believe no-one really has a satisfactory answer to as of when and how you can pare away complexity of quantum systems ... Where does one molecule end and another begin? Are we not all part of one gigantic molecule?)

For a system of $N$ interacting theoretical particles without internal degrees of freedom (spin), the wavefunction is
$$ \Psi(\mathbf{r}_1,\mathbf{r}_2,\cdots,\mathbf{r}_N,t) $$
Using the probabilistic interpretation, $|\Psi|^2$ is the probability of finding *all* the particles at the *configuration* $(\mathbf{r}_1,\cdots,\mathbf{r}_N)$ at time $t$. Quantum mechanics applied to multiple constituent particles does *not* describe individual particles, but *all of them at the same time*! This is an immensely complicated mathematical object in general. (See exercise below.)

Elementary particles usually also have internal degrees of freedom, *spin*. Thus, a single-particle configuration is rarely just a point $\mathbf{r} = (r_1, r_2, r_3)$ in space. Rather, we have a single-particle configuration
$$ x = (\mathbf{r}, \sigma), \quad $$
where $\sigma$ counts the spin eigenvalues. For electrons and other spin-1/2 particles, $\sigma$ is either $\alpha$ or $\beta$. So, we must specify *where* a particle is in space and *which direction spin points in*. 

The total wavefunction for $N$ particles becomes
$$ \Psi(x_1,x_2,\cdots,x_N,t) $$
Here $x_i = (\mathbf{r}_i, \sigma_i)$ is the space-spin configuration of a single electron, and $\sigma_i$ is either $\alpha$ or $\beta$. Since spin is discrete, so we have $2^N$ different combinations of spin, and for each combination of spins we have a space wavefunction!

Since spin-1/2 particles are *fermions* we also have the fundamental restriction that
$$ \Psi(x_1,x_2,\cdots,x_N,t) \quad \text{antisymmetric} $$
upon permutation of particle labels. The permutation antisymmetry is often called *The Pauli Principle*, and is very important to get the physics right.


## 2.2 Clamped nuclei approximation


The $N$-electron wavefunction is the quantum state of a **molecule in the clamped-nuclei approximation.** That is, the nuclei are fixed in space (we ignore that they are quantum particles), and the electrons are quantum mechanical, free to move around in space under the influence of $V(\mathbf{r})$ set up by the nuclei, and interacting among themselves with a repulsive Coulomb potentual. The electrons are **indistinguishable particles**, which means that it is **not possible nor meaningful** to say that electron 1 does that, and that electron 2 does another thing. Instead, we can only say what the **ensemble** of particles does.


I cannot stress properly the woes that the indistinguishability of the electrons and the  antisymmetry of the wavefunction cause for us quantum chemists. Indeed, one can say that it is the reason for our existence. Wavefunction antisymmetry is extremely complicated.

The TDSE for a molecule in the clamped nuclei approximation is still of the form previously seen, only that $\hat{H}(t)$ and the wavefunction are now more complicated. 

$$ i\hbar \partial_t \Psi(1,\cdots,N,t) = \hat{H}(t)\Psi(1,\cdots,N,t) $$

Here I introduced a shortcut notation.

With an external electric field in the *dipole approximation*, we have this Hamiltonian operator:
$$ \hat{H} = \sum_{i}^N \hat{T}(i) + \sum_i^N \hat{V}(i) + \sum_i^N \hat{U}(i,t) + \sum_{i,j;\;i<j}^N \hat{W}({ij}) $$
$$ \hat{T}(i) = -\frac{\hbar^2}{2 m_e} \nabla_i^2 $$
$$ \hat{W}(ij) = \frac{kq^2}{|\mathbf{r}_i - \mathbf{r}_j|} $$
$$ \hat{V}(i) = \sum_{A}^{N_\text{at}} \frac{-k q^2 Z_A}{|\mathbf{r}_i-\mathbf{R}_A|} $$
$$ \hat{U}(i,t) =  -q \mathcal{E}(t)\cdot \hat{\mu}(i)$$

Here, $\hat{\mu}(i)$ is th electric dipole operator acting on electron $i$, $q$ is the electron charge, $\mathcal{E}(t)$ is the electric field strength, $\mathbf{R}_A$ is the position of nucleus $A$, $-qZ_A$ is its charge.

**In electron dynamics, this is the relevant Hamiltonian**. Thus, we want to solve the TDSE for an electronic system starting out typically in the ground state. The wavefunction is governed by the TDSE, and an external electric field will move it away from the ground state, to an extent which depends on the electric field.


> [!exercise] Discussion in pairs
> In our 1d simulations, we used $n_g$ grid points to resolve the wavefunction. What is the cost of a 3d wavefunction with spin, i.e., the number $K$ of complex numbers that must be stored? 
> 
> Ignore antisymmetry, and estimate $K(N)$ for $N$ electrons.
> 
> Now, take antisymmetry into account for a more conservative $K(N)$. Hint: Binomial factors
> 
> Estimate $K(10)$ with $n_g = 10$. This is a *very* coarse approximation.
> 



Now comes the punchline of this section: **Considering the above exercise, it is clearly not feasible to use grid discretizations of $N$-electron wavefunctions.**



## 2.3 The Born--Oppenheimer approximation and our chemical intuition



The electron dynamics setting described above differs in several respects from the usual intuition we have in chemistry. By far, the common assumptions in computational chemistry amount to the following:
- *The Born--Oppenheimer approximation.* Nuclei are much heavier than electrons, and therefore move more slowly. Electrons react instantaneously to changes in nuclear positions, and are always in the ground state (on rare occasions, in excited states) by the adiabatic approximation.
- The concept of a *potential energy surface* (PES). The BO approximation implies that nuclei experience a potential energy from the electronic state, since this state is entirely determined by the nuclear positions. Electrons are *never considered to be moving*.
- The concept of a PES is **tied** to the adiabatic approximation, it does not make sense without it


- The nuclei are usually described *classically*. They have a definite position, usually near or at the equilibrium geometry. If quantum mechanics is involved, it is usually via the *harmonic approximation* where the PES is approximated with a quadratic polynomial near the equilibrium geometry.
- Chemistry happens on the PES: Chemical reactions can basically be thought of as nuclei moving about on the PES.


- *The single-particle picture.* Electrons are most often thought about as an atomic orbital, and vice versa. The totality of the electrons in the molecule are thus associated with unique orbitals due to the Pauli principle. (Just take a look at the periodic table of elements!) This is a testament to the uncanny success of the Hartree--Fock, and in later decades, the Kohn--Sham DFT method, where the working wavefunction is uncorrelated. The orbitals are responsible for *bonding* of the atoms.

The combination of these elements results in a mental picture compatible with a ball-and-stick model (showing the classical nuclear configuration of the molecule and the bonds), with isosurfaces of molecular orbitals superimposed. If the nuclear configuration is deformed, the orbitals are deformed accordingly. Here is an example of such a *molecular orbital plot*:


Tafrishi, R.; Torres-Diaz, D.; Amiaud, L.; Lafosse, A.; Ingólfsson, O. Low-Energy Electron Interaction with 2-(Trifluoromethyl)Acrylic Acid, a Potential Component for EUVL Resist Material. _Physical Chemistry Chemical Physics_ **2023**, _25_ (27), 17987–17998. [https://doi.org/10.1039/D3CP01860A](https://doi.org/10.1039/D3CP01860A).

![[orbitals2.gif]]


Kotaru, S.; Pokhilko, P.; Krylov, A. Spin–Orbit Couplings within Spin-Conserving and Spin-Flipping Time-Dependent Density Functional Theory: Implementation and Benchmark Calculations. ChemRxiv October 17, 2022. [https://doi.org/10.26434/chemrxiv-2022-jktk8](https://doi.org/10.26434/chemrxiv-2022-jktk8).

https://chemrxiv.org/engage/chemrxiv/article-details/634af02e4b0af38092c695c8

![[orbitals1.png]]



Contrast the above view with the exact time evolution of the $N$-electron function when, say, we have an intense laser pulse applied:
- The electron wavefunction is time-dependent. In particular, the simple orbital picture may be destroyed. Electrons may be **significantly, or even strongly, correlated**.
- Electrons cannot be identified with orbitals anymore
- The wavefunction may develop **very high levels of detail** not easily visualized. In particular it is *complex-valued*
- Since the electrons move away "from their orbitals", **molecular bonds may be broken, even if the nuclei are stationary at the time scale of the electrons.**
- Electrons may in fact escape (ionization). In the BO approximation this is usually taken to be an instantaneous process.


# 3 Many-electron dynamics methods

## 3.1 Some references

The literature is vast. Here are some general references that can be interesting to look at.


Li, X.; Govind, N.; Isborn, C.; DePrince, A. E.; Lopata, K. Real-Time Time-Dependent Electronic Structure Theory. _Chemical Reviews_ **2020**, _120_ (18), 9951–9993. [https://doi.org/10.1021/acs.chemrev.0c00223](https://doi.org/10.1021/acs.chemrev.0c00223).

Sverdrup Ofstad, B.; Aurbakken, E.; Sigmundson Schøyen, Ø.; Kristiansen, H. E.; Kvaal, S.; Pedersen, T. B. Time‐dependent coupled‐cluster Theory. _WIREs Comput Mol Sci_ **2023**, e1666. [https://doi.org/10.1002/wcms.1666](https://doi.org/10.1002/wcms.1666).

Hochstuhl, D.; Hinz, C. M.; Bonitz, M. Time-Dependent Multiconfiguration Methods for the Numerical Simulation of Photoionization Processes of Many-Electron Atoms. _The European Physical Journal Special Topics_ **2014**, _223_ (2), 177–336. [https://doi.org/10.1140/epjst/e2014-02092-3](https://doi.org/10.1140/epjst/e2014-02092-3).


Miyagi, H.; Madsen, L. B. Time-Dependent Restricted-Active-Space Self-Consistent-Field Theory for Laser-Driven Many-Electron Dynamics. _Phys. Rev. A_ **2013**, _87_ (6), 062511. [https://doi.org/10.1103/PhysRevA.87.062511](https://doi.org/10.1103/PhysRevA.87.062511).


## 3.2 Uncorrelated wavefunctions: Determinants

How do we deal with the enormous complexity of the $N$-electron wavefunction? 

In electronic structure theory, the Hartree--Fock and DFT approximations are enormously successful. We now consider time-dependent versions of these approaches, resulting in what is known as *time-dependent Hartee--Fock* and *time-dependent DFT*.

In Hartree--Fock, the $N$-electron wavefunction is written in terms of $N$ spin-orbitals

$$ \Psi(x_1,\cdots,x_N) = [\phi_1\phi_2\cdots \phi_N] $$
Here, 
$$ \phi_i(x) $$
is a spin-orbital, and the total wavefunction on the RHS is *an antisymmetric tensor product*. 

*This is the simplest way to obtain a many-electron wavefunction that obeys the Pauli principle.*

We here take a 2-electron example. The *tensor product* of $\phi_1$ and $\phi_2$ is simply
$$ \phi_1(x_1)\phi_2(x_2). $$
It is a 2-electron wavefunction obtained by taking the simple product of two functions. However, it is not antisymmetric: if I exchange $x_1$ and $x_2$, nothing is seen to "happen". To make the wavefunction antisymmetric, one uses a well-defined *antisymmetrization procedure*. For the $N=2$ case we write
$$ [\phi_1\phi_2] = \frac{1}{\sqrt{2!}}\left( \phi_1(x_1)\phi_2(x_2) - \phi_2(x_1)\phi_1(x_2) \right) $$
The Pauli principle is now in effect, meaning that if we exchange two spin-orbitals $\phi_i$ and $\phi_j$, the wavefunction gets a $-$ sign. Similarly, if I exchange the spin/space positions $x_i$ and $x_j$, I will get a minus sign. The generalization of this procedure to the $N$-fold tensor product is a *determinant*,
$$ [\phi_1\cdots \phi_N] = \frac{1}{\sqrt{N!}} \begin{vmatrix} \phi_1(x_1) & \cdots & \phi_1(x_N)  \\ \vdots & \phi_i(x_j) & \vdots \\ \phi_N(x_1) & \cdots & \phi_N(x_N) \end{vmatrix} $$
Don't worry if this feels very abstract -- after all, we have particle coordinates $x_i$ that are both space and spin coordinates, and we have matrix determinants, normalization factors ... The point here, is that we have a *well-defined procedure* to make the simplest possible $N$-electron wavefunctions that obey the Pauli principle.

Note also that the Pauli principle is manifested as antisymmetry when we move the spin-orbitals around on the left-hand side, due to the properties of the determinant. This reflects that the orbital indices are a kind of single-particle coordinate. We can say that the electrons are in the configuration given by spatial positions, or spin-orbital occupancies.



## 3.3 Time-dependent Hartree--Fock

In time-dependent Hartree--Fock, we use a single determinant function, and let the spin-orbitals $\phi_i$ be time-dependent. This draws us closer to the usual chemical intuition. Note however, that *the spin-orbitals, time-dependent molecular orbitals (MOs), become very complicated complex functions.* They should not be confused with the regular MOs.

In order to do actual computations, one must make a choice: How do we represent $\phi_i(x)$? We briefly discuss 2 main options:

**AO basis sets**

For a quantum chemist, the most immediate choice is to *expand $\phi_i$ in atomic orbitals (AOs).* These are the quantum chemical basis sets, usually gaussian-type orbitals (GTOs). Carefully optimized integral codes for electronic-structure theory can then be reused. Using the linear combination of atomic orbitals (LCAO) approach, we then get the *ansatz*:
$$ \phi_i(\mathbf{r},\sigma) = \sum_{\alpha} g_\alpha(\mathbf{r}) C_{\alpha i}^\sigma $$
where the spin dependence is put on the linear coefficients. The unknown of the TDHF approximation is then the coefficient matrix/tensor $C_{\alpha i}^\sigma$.

**Grid representations**

The second option is to resolve the time-dependent MO using a grid, similar to the simulations we have seen previously.

$$ \phi_i(\mathbf{r},\sigma) \approx \phi_i(\text{grid points}, \sigma) $$


There are many ways to do grid representations, and we will not talk about them here.


## 3.4 Comparison of AO vs grid approach

Let us briefly compare the grid and AO approaches to resolving time-dependent MOs.

AO basis sets:
- Tailored towards ground-state calculations
- With increasing size allows accurate description of some excited states
- Localized in space
- Cannot describe ionization processes
- Compact wavefunction storage (relatively speaking!)
- Dense operator representations (slow, not parallelizable)
 
Grid basis:
* Resolution limited by size of domain and mesh width
* Not tailored towards ground-state calculations
* Not localized in space
* Can describe ionization, motion far away from ground state
* Not compact wavefunction storage
* Sparse operator representations (fast, parallelizable)

Let me remark that the schism presented here is a little artificial. There are more general ways to represent the wavefunctions, and in practice, grids are not as simple as one might get the impression of here.


### 3.4.1 Visualization: Grid simulation vs. AO simulation


Example: 1d hydrogen atom in a finite gaussian basis vs. grid basis. Weak field vs. strong field.

## 3.5 Two regimes of applicability

The choice between AOs and grid-based MOs leads to a branching of the various time-dependent electronic structure methods. Grid based resolutions can describe much more involved dynamics, but is usually also more expensive. On the other hand, there are applications of electron dynamics that do not need this flexibility:

**If the wavefunction will stay close to the ground-state wavefunction, AOs are usually sufficient!**

For example, when the system starts out in the ground state and is perturbed by a weak perturbation, we will only get small oscillations of the ground state, perfectly described captured with the LCAO approach for the MOs. **This is the domain of response theory!** But in response theory, the quantities of interest are derived with only *implicit* reference to the TDSE - the TDSE is never actually *solved* numerically. Instead the various response functions are computed from, say, large-scale eigenvalue problems.



# 4 Time-dependent electronic structure theory
## 4.1 Time-dependent configuration interaction method

In electronic structure theory, a gaussian AO basis set is the starting point, and a set of occupied and unoccupied MOs are usually computed once and for all using the HF method. The HF determinant is uncorrelated. A *correlated wavefunction* is built from the MO basis set by taking combinations of the HF determinant and other determinants built using at least one unoccupied orbital (above the HOMO-LUMO gap).

This idea lends itself to the solution of the TDSE as well: we can take the MOs fixed (time-independdent), and consider a wavefunction of the form
$$ \Psi(t) = \sum_I c_I(t) \Phi_I. $$
Here, $I$ is a *configuration*, that is a selection of MOs (below the HOMO-LUMO gap or above), and $\Phi_I$ is the corresponding determinant. For example, $\Phi_0$ is usually the HF determinant, and we can consider singly excited, doubly excited, and so on, determinants. The sum does not usually run over *all* determinants due to the exponential scaling of the resulting linear space. (That would be full CI, FCI.)

Let us pause a bit to consider the meaning of a sum like the above, where we for clarity assume the sum to have $m$ terms:
$$ \Psi(t) = c_{1}(t) \Phi_1 + c_2(t) \Phi_2 + \cdots + c_m(t) \Phi_m $$
Taking a sum like this allows us to express that the system may be in each of the configurations $\Phi_I$ with *amplitude* $c_I(t)$. The total wavefunction is therefore a *Schrödinger's cat state*, and $\Psi(t)$ is an element of an $m$-dimensional vector space. During time evolution, the amplitudes will change, signifying different amplitudes for each configuration. *We see that the linear combination allows greater flexibility in the wavefunction*, and can describe electrons in different configurations.

Using the above wavefunction in the TDSE leads to the *time-dependent configuration-interaction model.* The coefficients $c_I(t)$ are time-dependent CI expansion coefficients, while the $\Phi_I$ are time-independent determinants, with electrons occupying different MOs.

Inserted into the TDVP, the CI expansion coefficients satisfy a differential equation on the form
$$ i \frac{d}{dt}\mathsf{c}(t) = \mathsf{H}(t) \mathsf{c}(t). $$
Here, $\mathsf{c}(t)$ is the expansion vector, and $\mathsf{H}(t)$ is the CI Hamiltonian matrix. This equation looks a lot like the TDSE, except that now we don't have a wavefunction, but a CI expansion vector, and the Hamiltonian differential operator is a (possibly time-dependent) matrix.

If we *do* take into account *all* possible configurations with the given set of MOs, we have the *full CI*, FCI, wavefunction. In quantum chemistry, this is the exact wavefunction in the given basis set. However, the given basis set is not complete, in particular it has the shortcomings discussed earlier, so it cannot simulate, for example, ionization of molecules. So, it is misleading to claim that TD-FCI is "exact" when AO basis sets are used.


> [!exercise] Discussion in pairs
> In a time-dependent FCI approximation, we have a set of $M$ spin-orbitals and $N$ electrons. What is the number of unique (that is, linearly independent) determinants that can be built with these spin-orbitals? (Hint: Recall the Pauli principle.) Do you think a time-dependent FCI is feasible? How does your estimate compare to the fully grid-resolved $N$-electron wavefunction form a previous exercise? Can you explain the similarity?

## 4.2 Time-dependent coupled-cluster method

CI theory is simple, but it is not *size extensive*: Application of the theory to molecular fragments A and B leads to different results than the theory applied to the combined molecule AB. In electronic-structure theory, *coupled-cluster theory* is usually perfered over CI. In CC theory, the wavefunction is written on exponential form,
$$ \Psi(t) = \exp[T(t)]\Phi_{HF}. $$
The symbol $T(t)$ is a *cluster operator,* and acting on $\Phi_{HF}$ it simply generates a CI-type expansion,

$$T(t)\Phi_{HF} = \sum_I \tau_I(t) \Phi_I. $$

By the magic of second-quantization formalism, the repeated action of $T(t)$ can be defined, and hence the exponential, and hence $\Psi(t)$. The remarkable property of the CC wavefunction is that it is *multiplicatively separable*: When two molecular fragments $A$ and $B$ are individually described with the CC wavefunction
$$ \Psi_A(t) = \exp[T_A(t)]\Phi_{HF,A}, $$
$$ \Psi_B(t) = \exp[T_B(t)]\Phi_{HF,B}, $$
and the combined molecule is described with the wavefunction
$$ \Psi_{AB}(t) = \exp[T_{AB}(t)]\Phi_{HF,AB}, $$
then we obtain as a *theorem* that
$$ T_{AB}(t) = T_A(t) + T_B(t). $$
Thus, we obtain the same answer by doing two smaller simulations and combining the results as doing a larger simulation.

As for the equations of motion for the *cluster amplitudes* $\tau_i(t)$, we have the interesting-looking equation
$$ i \frac{d}{dt} \tau_I(t) = \braket{\Phi_I| e^{-T(t)} H(t) e^{T(t)} |\Phi_{HF}} $$
In addition, CC theory is **nonvariational** which means that we cannot use the TDVP to generate its equations of motion (!). We must also solve a for a Lagrange multiplier cluster amplitude set $\lambda_I(t)$ using a **dual equation**.  Both equations are obtained from a **principle of stationary action that generalizes the TDVP**.
$$ \delta \mathcal{S} = 0 $$
with 
$$ \mathcal{S} = \int_{t_0}^{t_1}  \braket{\Phi_{HF}|(1 + \Lambda(t)) e^{-T(t)}\left[i \frac{d}{dt} - H(t) \right] e^{T(t)}|\Phi_{HF}} \; dt $$

I show these equations to hint at the mathematical structure of TDCC, and do not expect you to to understand this at a deep level. For me, the interesting and beautiful aspects of this theory lies in the fact that it is a variational formulation of exact quantum mechanics that allow more general and size-extensive treatments than the usual variational methods, like HF and CI. Indeed, we can write the exact TDSE using a functional $\mathcal{S}$ as $\delta \mathcal{S}=0$, with
$$ \mathcal{S} = \int_{t_0}^{t_1}  \braket{\tilde{\Psi}(t)|\left[i \frac{d}{dt} - H(t) \right] |\Psi(t)} \; dt $$
where we have *two independent* wavefunctions that can be *approximated in different ways using carefully selected schemes*. This method is called *the time-dependent bivariational principle* and generalizes the TDVP, see [[#1.7 Action principle|1.7 Action principle]].

# 5 Time-dependent MOs
## 5.1 Release the MOs! TD-RASSCF

In the TDHF method, the MOs were time-dependent, but the wavefunction only had a single configuration:
$$ \Psi_{TDHF}(t) = \Phi_{HF}(t) = [\phi_1(t) \cdots \phi_N(t)] $$
The MOs have maxium flexibility, and can move around and explore space. However, the wavefunction remains uncorrelated.

On the other hand, TDCI had time-fixed MOs, but variable amplitudes,
$$ \Psi_{TDCI}(t) = \sum_I c_I(t) [\phi_{I_1} \cdots \phi_{I_N}] $$
Here, the configuration $\Phi_I$ is written out in terms of MOs. The MOs are fixed, but the wavefunction can explore the different configurations.

**What if we combine the two approaches?** 

This would give us a very flexible wavefunction: A linear combination of time-varying configurations! The resulting method can be dubbed time-dependent restricted-active space self-consistent field (TD-RASSCF), or  multi-configuration time-dependent Hartree--Fock (MCTDHF), with wavefunction. Recall that a RAS is a recipe for distributing electrons in active spin-orbitals, i.e., a selection scheme for configurations. The total wavefunction reads
$$ \Psi_{TDRASSCF}(t) = \sum_{I\in\text{RAS}} c_I(t) [\phi_{I_1}(t) \cdots \phi_{I_N}(t)] $$
and has become a very important framework for electron dynamics, especially when coupled with *grid based resolutions* of the MOs. 

Again, equations of motion are mathematically derived using the TDVP. The equation from TDCI is now extended with time-dependent mean-field equations for the various active spin-orbitals $\phi_i(x,t)$. 


## 5.2 OA-TDCC, OO-TDCC

A similar approach can be taken with TDCC: The MOs can be released and the bivariational principle will dictate their equations of motion. There are two main flavors of such a theory: (1) Orbital-adaptive TDCC (OATDCC), and orbital-optimized TDCC (OO-TDCC or TD-OCC). These similar-sounding methods are subtly different.

In TD-OCC, the orbitals are assumed *always orthonormal*. It is therefore a time-dependent version of *orbital-optimized CC*. However, optimizing a single orthonormal set of orbitals is not the "right" way to do it according to the TD-BIVP, and it is known to not converge to full CI as the cluster operator approaches completeness. Rather, a **two sets of biorthogonal orbitals** should be used, which is of course a complication.

The resulting equations and their derivation formally resemble MCTDHF, and the standard TDCC amplitude equations are extended with mean-field like equations for the time-dependent MOs.

Again, the methods branch into different methods based on whether AOs or a grid representation is used for the MOs.

## 5.3 Summing up in a table

We can sum up the various time-dependent electronic structure theory methods in a table:

| Static MOs | Time-dependent MOs |
| ------- | -------- |
| (HF) | TDHF |
| TD(RAS)CI | TDRASSCF/MCTDHF |
| TDCC | OATDCC/TDOCC |
| TDFCI | (TDFCI) |


In this table, correlation level is increasing downwards. With static MOs, Hartree--Fock has no degree of freedom left, so the method is trivial. When the MOs are time-dependent we obtain TDHF (to the right). Allowing a set of configurations gives us TDCI with static MOs. The CI configuration selection scheme can be arbitrary; typically a RAS. If we allow the MOs to evolve, we obtain TDRASSCF or MCTDHF.

If we move to CC theory, then TDCC has static orbitals, and is thus similar to TDCI. With moving MOs we get OATDCC or TDOCC, depending on flavor.

Finally, we have TDFCI, where every single configuration with the available set of MOs is used. This implies that there are no degrees of freedom left if we let the MOs be time dependent -- every change in an MO can be rewritten as a change in the CI coefficients! Thus, to the right, we have in fact TDFCI as well!



# 6 THE END



