# Hylleraas School 2024 - Electron Dynamics

## About the lectures

The lectures are written with a generalist computational chemistry target group in mind. The focus is on general concepts and broad understanding, rather than complicated mathematics and derivations. There are plenty of visualizations in the form of animated solutions to the time-dependent Schrödinger equation.

A list of learning goals are as follows:
* Learn about the time-dependent Schrödinger equation (TDSE)
* Be able to interpret the complex-valued quantum mechanical wavefunction
* Understand the challenges of computational attochemistry
* Be able to separate chemical intuition from electron dynamics 
* Understand the challenges in solving the many-electron TDSE on a computer
* Learn about the most popular methods for approximating many-electron dynamics
* Be able to relate the TDSE to material previously taught, such as response theory, spectroscopy
* Understand the difference between grid-based and atomic-orbital based calculations
* Get an overview of real-time electronic structure theory methods
* Get hand-on experience with solving TDSE for both simple model problems and actual molecular systems


## Lectures

The lecture slides and script can be found here:
- Slides Part 1 [electron-dynamics-1.pptx](./slides/electron-dynamics-1.pptx)
- Slides Part 2 [electron-dynamics-2.pptx](./slides/electron-dynamics-2.pptx)
- Lecture script, PDF: [lecture-script.pdf](./script/lecture-script.pdf)
- Lecture script, source: [lecture-script.md](./script/lecture-script.md)

## Animations

Here is the complete list of animations shown in the lectures, with Jupyter Notebooks that generate the animations.

### Particle in gravity field
- Jupyter Notebook: [gravity.ipynb](./animations/gravity.ipynb)
- Complex visualization: [gravity_complex.mp4](./animations/gravity_complex.mp4)
- Probability visualization: [gravity_magnitude.mp4](./animations/gravity_magnitude.mp4)

### Double slit experiment
- Jupyter Notebook: [double_slit.ipynb](./animations/double_slit.ipynb)
- Double slit experiment: [double_slit.mp4](./animations/double_slit.mp4)
- Single slit experiment: [single_slit.mp4](./animations/single_slit.mp4)

### 1d atom model
- Jupyter Notebook: [atom_1d.ipynb](./animations/atom_1d.ipynb)
- Weak field: [atom_1d_weak.mp4](./animations/atom_1d_weak.mp4)
- Strong field: [atom_1d_strong.mp4](./animations/atom_1d_strong.mp4)
- Weak field, eigenfunction basis: [atom_1d_weak.mp4](./animations/atom_1d_eigenbasis_weak.mp4)
- Strong field, eigenfunction basis: [atom_1d_weak.mp4](./animations/atom_1d_eigenbasis_strong.mp4)

### Harmonic oscillator in 1d and 2d
- Jupyter Notebook: [harmonic_oscillator_1d.ipynb](./animations/harmonic_oscillator_1d.ipynb)
- Jupyter Notebook: [harmonic_oscillator_2d.ipynb](./animations/harmonic_oscillator_2d.ipynb)
- Ground state 1d: [ho_1d_0.mp4](./animations/ho_1d_0.mp4)
- Excited state 1d: [ho_1d_1.mp4](./animations/ho_1d_1.mp4)
- Excited state 2d: [ho_2d_1_0.mp4](./animations/ho_2d_1_0.mp4)
- Highly excited state 2d: [ho_2d_10_2.mp4](./animations/ho_2d_10_2.mp4)


### Reversibility of time-dependent Schrödinger equation
- Jupyter Notebook: [hylleraas.ipynb](./animations/hylleraas.ipynb)
- Movie: [hylleraas.mp4](./animations/hylleraas.mp4)


## Tutorial notebooks

There are 3 tutorial notebooks with documentation:
- Documentation: [tutorials.md](./tutorials/tutorials.md)
- Tutorial 1 - High-Harmonic Generation in 1d: [hhg_1.ipynb](./tutorials/hhg_1d.ipynb)
- Tutorial 2 - Molecular dynamics of $H_2$: [morse.ipynb](./tutorials/morse.ipynb)
- Tutorial 3 - Absorption spectra with TDCIS: [tdcis.ipynb](./tutorials/tdcis.ipynb)

At the Hylleraas School, the notebooks will be available at the NIRD platform/JupyterHub. In the notebooks, there are hyperlinks to the documentation.




