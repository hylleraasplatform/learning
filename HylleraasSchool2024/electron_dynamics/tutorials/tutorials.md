# Table of contents
- [Table of contents](#table-of-contents)
- [Tutorial 1. High-Harmonic Generation in a 1D model potential](#tutorial-1-high-harmonic-generation-in-a-1d-model-potential)
  - [Important information](#important-information)
  - [About the simulation](#about-the-simulation)
  - [High-Harmonic Generation](#high-harmonic-generation)
  - [Tasks](#tasks)
    - [Get acquainted](#get-acquainted)
    - [Prepare for tasks](#prepare-for-tasks)
    - [TASK 1. A new model potential](#task-1-a-new-model-potential)
    - [TASK 2. Changing the laser pulse](#task-2-changing-the-laser-pulse)
    - [TASK 3. Give animation a new name](#task-3-give-animation-a-new-name)
    - [Rerun notebook](#rerun-notebook)
    - [Discussion](#discussion)
- [Tutorial 2. Molecular dynamics of $H\_2$](#tutorial-2-molecular-dynamics-of-h_2)
  - [Important information](#important-information-1)
  - [The model](#the-model)
  - [Tasks](#tasks-1)
    - [Get acquainted](#get-acquainted-1)
    - [Prepare for tasks](#prepare-for-tasks-1)
    - [TASK 1.](#task-1)
    - [TASK 2.](#task-2)
    - [TASK 3.](#task-3)
    - [TASK 4.](#task-4)
    - [Discussion](#discussion-1)
- [Tutorial 3. Absorption spectra using TD-CIS](#tutorial-3-absorption-spectra-using-td-cis)
  - [Important information](#important-information-2)
  - [About the simulation](#about-the-simulation-1)
  - [Tasks](#tasks-2)
    - [Get acquainted](#get-acquainted-2)
    - [TASK 1.](#task-1-1)
    - [TASK 2.](#task-2-1)
    - [TASK 3.](#task-3-1)


# Tutorial 1. High-Harmonic Generation in a 1D model potential


**Notebook: `hhg_1d.ipynb`**

This notebook contains a complete script to produce a 1d model simulation on a hydrogen-like atom. In this tutorial, you will modify the notebook to produce a simulation for a 1d model of the $H_2^+$ ion, and compare your results with published results!

The code in Tutorial 1 and Tutorial 2 uses [``fft_tdse``](https://github.com/simenkva/fft_tdse/), a toolbox made for doing basic simulations and visualizations as seen in the lectures. 


## Important information

<!--The notebook uses the simulation toolbox that was also used in the Electron Dynamics lectures, called `fft_tdse`, and must be installed with the following command in a terminal window:
```
pip install git+https://github.com/simenkva/fft_tdse.git
```-->

The Sigma2 JupyterLab solution can be a little fickle: If it runs out of memory, the current Jupyter kernel will simply die, without explanation and warnings. It is advisable to limit the number of running notebooks. (Each running notebook has a Jupyter kernel associated with them.) In the file overview, if there is a green dot next to a notebook file, a kernel is running. Right click to open a context menu from which you can shut down the kernel.

The Electron Dynamics notebooks are thoroughly tested, and do not seem to make the kernel die. If, however, you start to increase (a) the number of grid points, or (b) the animation frame size, you may run into trouble in this respect. Just be warned! 


## About the simulation

We now describe the initial simulation. You do not have to understand everything to solve the exercise. You will be guided in the tasks.

The notebook solves the time-depdenent Schrödinger equation for a single particle of mass $m=1$ and charge $q=-1$ (atomic units) in 1 spatial dimension. The particle "sees" an external potential $V(x)$ and a laser field $\mathcal{E}(t)$. For the 1d hydrogen model the potential is
$$ V(x) =  -\frac{1}{\sqrt{x^2 + 2}}. $$
You can see it plotted when you run the notebook, see the tasks below. The model potential $V(x)$ behaves like $\sim 1/x$ for large $|x|$, but for small $|x|$ it is a smooth function, amenable for simulation. The ground state energy of this potential is $-1/2$ Hartrees, just like for the "real" H atom. This implies that the ionization potential is the same in the model as in "real life", which means that the effects of the laser should at least be qualitatively comparable.

The laser pulse $\mathcal{E}(t)$ has several parameters, which we list here:
- Base frequency $\omega$ - the color of the laser.
- Number of optical cycles $n_{cycles}$ - the total number of cycles with frequency $\omega$.
- Peak electric field $\mathcal{E}_0$ - the strength of the laser.
- Envelope function, to modulate the optical cycles.
- Starting time of pulse $t_0$ - the pulse turns on at this time. It is turned off after $n_{cycles}$ optical cycles, at time $t_0 + T$.

The simulation gathers data for calculation of the High-Harmonic Generation (HHG) spectrum. The data gathered are the expectation values of the dipole operator for every time,
$$ \mathrm{dip}(t) = \langle\Psi(t)|x|\Psi(t)\rangle. $$
An acellerating dipole moment radiates electromagnetic waves (classically). Thus, computing the Fourier transform of the dipole acceleration gives information about the radiated spectrum.

## High-Harmonic Generation

The process of HHG is a fundamental tool for generating ultrashort laser pulses, but also for studying - in real time - the evolution of chemical reactions. (We then talk about HHG spectroscopy.) A standard picture of the HHG process is that of the **three step model** (images from "Introduction to High-Harmonic Generation" by C. Jin, https://doi.org/10.1007/978-3-319-01625-2_1):

<img src="https://gitlab.com/hylleraasplatform/learning/-/raw/master/HylleraasSchool2024/electron_dynamics/tutorials/3-step-model.webp?ref_type=heads" width="500px"/>

1. **Ionization:** The first step involves the ionization of an atom or molecule by an intense laser field. The strong electric field of the laser pulse is powerful enough to remove an electron from the atom, creating a positively charged ion.

2. **Acceleration:** After ionization, the freed electron is accelerated by the electric field of the laser. Due to the oscillatory nature of the laser field, the electron undergoes rapid acceleration and deceleration, gaining energy in the process.

3. **Recombination and Emission:** In the final step, the accelerated electron recombines with its parent ion. During this process, the electron releases its excess energy in the form of a high-energy photon. These emitted photons are high-order harmonics, with frequencies much higher than the frequency of the original laser.



The HHG spectrum is usually divided into several regions (note the log scale):
 
 <img src="https://gitlab.com/hylleraasplatform/learning/-/raw/master/HylleraasSchool2024/electron_dynamics/tutorials/hhg-schematic.webp?ref_type=heads" width="500px" />

The peturbative regime can be well-explained by perturbation theory. The plateau shows where significant higher harmonics are generated. One is typically interested in a large plateau, which means that one can get very high frequency light emitted from the atom. The cutoff region is where the higher harmonics fall off exponentially in intensity, and cannot be utilzied.


## Tasks

### Get acquainted

Run the script by running each cell in turn (SHIFT+ENTER). Try to read each code cell, and take note of code comments. The code should produce an animation `hhg_1d.mp4` which you can download from the file manager by right clicking on the file name and choosing "Download". A HHG spectrum is also plotted near the end, and saved to file with the name `hhg_1d_spectrum.pdf`.

With your peers, discuss the animation in terms of the three-step model. Is it possible to easily identify some of the steps? Also discuss the HHG spectrum in terms of the schematic representation presented above.

### Prepare for tasks

Now, save the notebook under a different name, so that the old notebook is not overwritten.

Each TASK below is clearly marked in the notebook to help you localize them, e.g.,
```python
# ----------------------
# TASK 1. 
# ----------------------
```


### TASK 1. A new model potential


The task is to change the model potential from the current one, which is a hydrogen atom model potential, to a potential that mimicks $H_2^+$:
$$ V(x) = -\frac{1}{\sqrt{(x-R/2)^2 + 1.44}} -\frac{1}{\sqrt{(x+R/2)^2 + 1.44}}. $$
Here, $R$ is the interatomic distance.

Run the corresponding cell, and observe the plot.

### TASK 2. Changing the laser pulse

The $H_2^+$ electron is more tightly bound to the two "nuclei" than the corresponding $H$ atom. Therefore, we increase the pulse strength and number of cycles: Set number of optical cycles to 10, and the intensity to $I = 10^{14} \;\mathrm{W\cdot cm^{-2}}$. Run the corresponding cell to see the new laser pulse.


### TASK 3. Give animation a new name

We are now going to remake the animation. Give the animation a new name so we avoid overwriting the previous animation and HHG plot. Locate the variable that stores the name of the animation and change it to something else.


### Rerun notebook

Run the remaining cells to the bottom and inspect the results. (Alternatively, restart the kernel and run the whole notebook to make sure that any residual entropy from your experimentation is eliminated.)

If you are not happy with the visualization, you can play with the options of the `Animator1d` object. In particular, you can zoom in on a region on the $x$-axis, if you so wish.

### Discussion

Watch the produced animation, and discuss its qualitative features relative to the original H simulation with the students in your group. Discuss and compare the HHG spectra. Can you identify the main regions?

The $H_2^+$ simulation is similar to a study published in:

> Labeye, M. et al, "Optimal Basis Set for Electron Dynamics in Strong Laser Fields: The Case of Molecular Ion $H_2^+$, *J. Chem. Theory Comput.* 2018, 14 (11), 5846–5858. https://doi.org/10.1021/acs.jctc.8b00656.

In Figure 4 in that article, the authors show the HHG spectrum for the same system as we presently study, computed using a variety of numerical wavefunction representations, including a (finite difference) grid (black curve), which is comparable to our simulation. Compare with your obtained spectrum, and pay close attention to the details. Note that you should not expect the spectra to match perfectly, since the numerical methods used are not the same. In particular, our cutoff region is not accurate.

The relevant panel in Figure 4 is reproduced here:
 
 <img src="https://gitlab.com/hylleraasplatform/learning/-/raw/master/HylleraasSchool2024/electron_dynamics/tutorials/figure4_jctc.png?ref_type=heads" width="500px" />





# Tutorial 2. Molecular dynamics of $H_2$

**Notebook: `morse.ipynb`**

In this tutorial, you will convert a 1d simulation of a *Morse potential* model of the $H_2$ molecule to 2d. This notebook is not strictly *electron* dynamics, but rather *molecular dynamics*, as the electronic PES is modeled with a Morse potential.


## Important information

This tutorial is slightly more advanced than Tutorial 1. Also, the simulation times in 2d on the JupyterHub is on the order of minutes, so a certain degree of patience must be expected. The results are very nice, though, and well worth the wait.

## The model

The model we study is an approximation to the $H_2$ molecule in the Born-Oppenheimer approximation. Two protons of mass $m = 1836$ interact using the Coulomb repulsion and they also see the electronic potential energy surface. Their coordinates are $\mathbf{r}_1$ and $\mathbf{r}_2$, giving a wavefunction $\Psi(\mathbf{r}_1,\mathbf{r}_2,t)$. This is much too complicated for grid calculations such as ours. Luckily, the potential is translation invariant, so we can move to the centre-of-mass coordinate system, where we obtain a 3d Schrödinger equation in terms of the relative coordinate $\mathbf{r} = \mathbf{r}_1-\mathbf{r}_2$ only. The mass of this "pseudoproton" is the reduced mass $\mu = 918.076341$. The charge of this pseudoproton is $q = 1$. 

In lieu of *ab initio* electronic-structure calculations for the electronic PES, we use a fitted Morse potential. The Morse potential as a function of radial distance $r$ is
$$ V(r) = D_e (1 - e^{-a(r-r_e)})^2. $$
Here, $r_e$ is the equilibrium distance, while $a$ and $D_e$ are other parameters that control the width and depth of the potential. In our model, we choose the parameters as:
$$ a = 1.4556 $$
$$ r_e = 1.4011 $$
$$ D_e = 0.17449 $$
These values are taken from 
> Kinghorn, D. B.; Adamowicz, L. A New N-Body Potential and Basis Set for Adiabatic and Non-Adiabatic Variational Energy Calculations. J. Chem. Phys. 1997, 106 (21), 8760–8768. https://doi.org/10.1063/1.473936.

Thus, our total Hamiltonian is
$$ H(t) = -\frac{1}{2\mu} \nabla^2 + V(r) + q x \mathcal{E}(t). $$
Here, $\mathcal{E}(t)$ is the electric field strength of a laser pulse aliged with the $x$-axis.
Unfortunately, we do not have the computing resources to attack the 3d problem presently, but we still obtain very interesting results with a 2d simulation. We build our 2d simulation starting from a simpler 1d simulation.


## Tasks

### Get acquainted

Run the script by running each cell in turn (SHIFT+ENTER). Try to read each code cell, and take note of code comments. The code should produce an animation `morse_1d.mp4` which you can download from the file manager by right clicking on the file name and choosing "Download". 

The laser chosen is pretty strong, and probably hard to make in the lab. We choose it for its interesting results in the 1d and 2d case, which demonstrates interesting aspects of time-dependent wavefunctions.

With your peers, discuss the animation in terms of evolution of the wavefunction. Note that the wavefunction consists of two more or less independent parts. Discuss this aspect in terms of $x$ as the relative coordinate, $x = x_1 - x_2$ of the 1d $H_2$ molecule.

### Prepare for tasks

Now, save the notebook under a different name, so that the old notebook is not overwritten.

Each TASK below is clearly marked in the notebook to help you localize them, e.g.,
```python
# ----------------------
# TASK 1. 
# ----------------------
```

### TASK 1.

Convert ``Simulator`` object to a 2d simulation. Read the comments in the appropriate cell for guidance. There are some subtasks. When done, make sure you run this cell, and also the next cell, which runs ``sim.prepare()``. This function computes the ground state using an iterative method, which requires some time using the alloted resources on the JupyterHub. It should use about 25 iterations, and 10 minutes in total.

Note that if you run ``sim.prepare()`` again, say if you want to produce the animation again, the ground state will *not* be computed again, since the simulator remembers the ground state!

### TASK 2.

Visualize the 2d wavefunction. The current visualization is for the 1d ground state, and for the 2d case we need different tools. It is suggested that you use ``phase_mag_vis2``. If you run ``help(phase_mag_vis2)`` in an empty cell (at the bottom of the notebook, for example), you will see a concrete example which you can copy almost verbatim. 

### TASK 3.

Replace ``Animator1d`` object with ``Animator2d`` object, and do other necessary modifications. Pay attention to the comments.

Note the optional change of visualization type. This will create quite different visualizations. For the discussion below, it is recommended that you have created a `complex` visualization (which is the default).

### TASK 4.


Run the simulation. No coding needed! This should now generate a 2d animation. It will take a few minutes (about 10),  but is be worth the wait. You will see a preview of the animation as the simulation runs.


### Discussion

Discuss the results with your peers. What happens during the pulse? What is the wavefunction immediately after the pulse? What happens afterwards? Interpret in terms of molecular dissociation, alignment, et.c., but keep in mind that the wavefunction is probabilistic.

You may find that you have "What if?"-type questions. Then rerun the simulation with different settings!




# Tutorial 3. Absorption spectra using TD-CIS


## Important information

**Notebook: `tdcis.ipynb`**

The present tutorial exercise is based in on the CIS code in the [HyQD toolbox](https://github.com/HyQD) (Hylleraas Quantum Dynamics). Many thanks to Håkon E. Kristiansen for help with making this exercise great. 


## About the simulation




In this tutorial, you will compute absorption spectra of small molecules using time-dependent CI singles, an affordable real-time electronic-structure theory method we covered in the lectures. Recall that in CIS, the wavefunction if of the form
$$ \Psi(t) = c_0\Phi_0 + \sum_{I} c_I \Phi_I, $$
where $\Phi_0$ is the HF state, and where $\Phi_I$ are *singly excited* Slater determinants, i.e., one occupied spin-orbital (HOMO and below) is traded for an unoccupied orbital (LUMO and above). The spin-orbitals are fixed.

The CI wavefunction is all right at describing small oscillations around the ground state, if the HF approximation is good.

Given a molecule that starts out in the ground state, we give it a small *kick* with a very brief but strong laser pulse with a given polarization direction. Indeed, the kick is infinitely brief -- a Dirac delta pulse. For a gas sample, the molecules are randomly aligned, so the polarization direction can be taken to be random, while the molecule is fixed. Thus, we have a time-dependent Hamiltonian
$$ H(t) = H_0 + \delta(t)\mathcal{E}_0 \hat{\mathbf{k}}_\alpha, $$
where $\hat{\mathbf{k}}_\alpha$, $\alpha \in \{x, y, z\}$ is the current polarization vector, i.e., the unit vector in each spatial direction.


Intuitively, the molecule will start to oscillate with a small amplitude, with frequencies determined by the dipole-allowed excited states. (The laser only couples the ground state to these.) Indeed, the absorption spectrum as function of photon frequency $\omega$ is given by the following expression:
$$ S(\omega) \propto \omega \operatorname{Im} \sum_{\alpha\in\{x,y,z\}} \mu_\alpha(\omega), $$
where $\mu_\alpha(\omega)$ is the Fourier transform of the dipole moment
$$ \mu_\alpha(t) = \langle\Psi(t)| \hat{\mu}_\alpha |\Psi(t)\rangle . $$


If we now solve the time-dependent Schrödinger equation with $\Psi(0) = \Phi_{HF}$ (the ground state), and compute the dipole moment $\mu_\alpha(t)$ as we simulate, repeating the simulation for all 3 directions $\alpha$, we have all the ingredients needed to compute $S(\omega)$. The first time step is special, since the laser pulse dominates here. In the remaning time steps, the laser pulse is identically zero. The longer we simulate, the more accurate the spectrum will be, since we can do a more accurate Fourier transform.


## Tasks

### Get acquainted

Run the script by running each cell in turn (SHIFT+ENTER). Try to read each code cell, and take note of code comments. The cell that runs PySCF (i.e., calls ``construct_pyscf_system_rhf``) is a little slow at the technical solution.

The code should produce a spectrum in the end, saved to a PDF file. In the notebook, the spectrum is plotted, but unfortunately the technical solution does not support zooming. To reduce the x-axis, change the variables ``min_freq`` and ``max_freq`` and rerun the current cell.


### TASK 1. 

In the first cell, a number of small molecules are defined. In the next cell, a molecule is selected. Experiment with different molecules. You can also change the basis set. Typical examples would include:
- ``sto-3g``
- ``cc-pvdz``
- ``cc-pvtz``
- ``aug-cc-pvdz``

### TASK 2.

Pick a small molecule, say ``lih`` and a small basis, say ``cc-pvdz``. Make sure that ``min_freq = 0`` and ``max_freq = np.inf`` in the plotting cell. Restart the kernel and run all cells.


The parameters of the time integration is the time step ``dt`` and the final time ``t_final``. Try to adjust these parameters, rerun the time integration and plotting, and observe the spectra. How do these parameters affect the quality of the spectra? 

### TASK 3.

Discuss among your peers: How can you systematically improve the accuracy of the computed spectra within the TD-CIS method? I.e., which parameters of the method and model can you adjust, and how will they affect the computing time and results? (In TASK 2 you adjusted some of the parameters, but there are others, too.)


