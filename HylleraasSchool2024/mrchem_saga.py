
import hylleraas as hsp
import hyif
import os
from datetime import timedelta

water = hsp.Molecule("O")

# --- Bin Gao, you need to update these two lines

user='simensr'
project='nn4654k' #Oslo
#project='nn14654k' #Tromsø


# --- Set up the Saga SLURM settings

cluster_dir=f'/cluster/work/users/{user}/rundir'

mrchem_env=dict()
mrchem_env['user']=user
mrchem_env['slurm_account']=project
mrchem_env['work_dir_local']=os.path.realpath('./')+'/school_wrk'
mrchem_env['data_dir_local']=os.getcwd()+'/school_wrk'
mrchem_env['submit_dir_remote']=cluster_dir
mrchem_env['work_dir_remote']=cluster_dir
mrchem_env['data_dir_remote']=cluster_dir
mrchem_env['modules']=["OpenMPI/4.1.5-GCC-12.3.0", 
                       " Python/3.11.3-GCCcore-12.3.0"]
mrchem_env['env']=["export PATH=$PATH:/cluster/shared/hylleraas/mrchem/bin",
                   "export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/cluster/shared/hylleraas/mrchem/lib64"]
mrchem_env['job_time']=timedelta(minutes=10)
mrchem_env['memory_per_cpu']=4000
mrchem_env['ntasks']=1
mrchem_env['cpus_per_task']=40
mrchem_env['work_dir_local']=os.path.realpath('./')
mrchem_env['data_dir_local']=os.path.realpath('./')

mrchem_compute_saga = hsp.create_compute_settings('saga', **mrchem_env)

# --- Here are all the MRChem specific things

# Change this to modify the MRChem accuracy
world_prec = 1.0e-4

# Available keys and values in `specific_input` can be found at https://mrchem.readthedocs.io/en/latest/users/user_ref.html
energy_dict = {
    'qcmethod': 'blyp',
    'specific_input': {
        'world_prec': world_prec,
        'SCF': {
            'guess_prec': 1.0e-3,
            'guess_type': 'sad_gto',
            'guess_screen': 12.0
        }, 
    },
    'check_version':False,
}

# --- Here you can change back to your local setting to compare results

mrchem_blyp = hyif.MRChem(energy_dict, compute_settings=mrchem_compute_saga)
energy = mrchem_blyp.get_energy(water)
print(energy)

# NMR shielding calculation using MRChem in parallel
nmr_dict = {
    'qcmethod': 'pbe',
    'specific_input': {
        'world_prec': world_prec,
        'Properties': {'nmr_shielding': True},
        'SCF': {
            'guess_prec': 1.0e-3,
            'guess_type': 'sad_gto',
            'guess_screen': 12.0
        },
        'NMRShielding': {
            'nuclear_specific': False,
            'nucleus_k': [0, 1]
        }
    },
    'check_version':False,
}
mrchem_nmr = hyif.MRChem(nmr_dict, compute_settings=mrchem_compute_saga)
# Only NMR shielding tensors of the first two atoms (specified by `nucleus_k` in `nmr_dict`) will be computed
tensor = mrchem_nmr.get_nmr_shielding(water)
for atom in [0, 1]:
    print(tensor[atom]['total'])
    print(tensor[atom]['dia'])
    print(tensor[atom]['para'])

